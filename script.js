/*Відповіді по теорії:
1. Типи даних: string (строка), number (число), boolean(логичний тип), undefined (невизначений тип),Symbol, bigInit, object, null.
2.== це означає "дорівнює". Не суворе. Оператор рівності приводить до одного типу, порівнює типи як вони є
=== це означає сувора рівність, строге порівняння. Порівнює величини без наведення, якщо величини різгих типів, поверне false навіть не порівнюючию
3. Оператори- це інструкція мови в JS для процусу обробки інформації. Є оператори порівняння, присвоєння, додавання,  умови if тa else, тернарний оператор, логічні оператори: || (АБО),  && (І), ! (НЕ) та іншші
*/

const ALLOWED_TO_VISIT_WEBSITE = 18;
const WANT_TO_CONTINUE = 22;

let userName = '';
let userAge = 0;
let isRepeat = true;

while(isRepeat) {
    userName = prompt('Are your name?', userName);
    if (!userName) {
        userName = '';
    }

    userAge = Number(prompt('Your age?', userAge));
    if (isNaN(userAge)) {
        userAge = 0;
    }

    if (userName.length > 0 && userAge > 0) {
        isRepeat = false;
    }
}

if (userAge < ALLOWED_TO_VISIT_WEBSITE) {
    alert('You are not allowed to visit this website');
} else if (userAge >= ALLOWED_TO_VISIT_WEBSITE && userAge <= WANT_TO_CONTINUE) {
    let result = confirm('Are you sure you want to continue?');
    if (result === true) {
        alert('Welcome ' + userName);
    } else {
        alert('You are not allowed to visit this website');
    }
} else if (userAge > WANT_TO_CONTINUE) {
    alert('Welcome ' + userName);
}
